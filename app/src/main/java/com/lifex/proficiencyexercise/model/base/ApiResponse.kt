package com.lifex.proficiencyexercise.model.base

data class ApiResponse<out T>(
        val title: String? = null,
        val rows: T? = null)