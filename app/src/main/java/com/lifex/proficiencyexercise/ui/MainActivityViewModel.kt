package com.lifex.proficiencyexercise.ui

import androidx.lifecycle.ViewModel
import com.lifex.proficiencyexercise.model.RowsItem
import com.lifex.proficiencyexercise.model.base.ApiResponse
import com.lifex.proficiencyexercise.repo.MainRepository
import com.lifex.proficiencyexercise.repo.utils.*
import com.lifex.proficiencyexercise.utils.NetworkHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainActivityViewModel(
    private val networkHelper: NetworkHelper,
    private val mainRepository: MainRepository
) : ViewModel() {


    val newsResponse = SingleLiveEvent<Resource<List<RowsItem>>>()

    private val compositeDisposable = CompositeDisposable()


    fun getNews() {
        if (!networkHelper.isNetworkConnected()) { //It is true as we mocked in Test Class
            newsResponse.value = Resource.error(AppError.NetworkError)
            return
        }
        newsResponse.value = Resource.loading()
        // Crashing here as we are assigning value to live data newsResponse
        val api = mainRepository.getNews()

        val com = api.subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    handleResults(result)
                },
                { error ->
                    handleError(error)
                },
            )
        compositeDisposable.add(com)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    private fun handleResults(response: ApiResponse<java.util.ArrayList<RowsItem>>) {
        if (response.rows != null) {
            newsResponse.value = Resource.success(response.rows)
        } else {
            newsResponse.value = Resource.error(AppError.CommonError)
        }
    }

    private fun handleError(t: Throwable?) {
        newsResponse.value = Resource.error(t?.failureAppError() ?: AppError.CommonError)
    }


}
