package com.lifex.proficiencyexercise.utils

import android.view.View
import com.google.android.material.snackbar.Snackbar

fun View.visible(){
    visibility = View.VISIBLE
}
fun View.gone(){
    visibility = View.GONE
}
fun View.snackBar(message:String){
    Snackbar.make(this,message,Snackbar.LENGTH_LONG).show()
}