package com.lifex.proficiencyexercise.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.lifex.proficiencyexercise.TestSchedulerProvider
import com.lifex.proficiencyexercise.model.RowsItem
import com.lifex.proficiencyexercise.model.base.ApiResponse
import com.lifex.proficiencyexercise.repo.MainRepository
import com.lifex.proficiencyexercise.repo.utils.AppSchedulerProvider
import com.lifex.proficiencyexercise.repo.utils.Resource
import com.lifex.proficiencyexercise.utils.NetworkHelper
import io.reactivex.Observable
import junit.framework.TestCase
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import java.util.ArrayList

@RunWith(MockitoJUnitRunner::class)
class MainActivityViewModelTest : TestCase() {


    private val networkHelper = Mockito.mock(NetworkHelper::class.java)

    private val mainRepository = Mockito.mock(MainRepository::class.java)

    private val viewModel = MainActivityViewModel(networkHelper, mainRepository)

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()


    @Before
    public override fun setUp() {

        // MockitoAnnotations.initMocks(this)
    }

    @Test
    fun getNews() {
        Mockito.`when`(mainRepository.getNews()).thenReturn(Observable.just(getMockResponse()))
        //It is working good
        Mockito.`when`(networkHelper.isNetworkConnected()).thenReturn(true)

        //It is working good
        viewModel.getNews()

        assertEquals(Resource.success(getList()), viewModel.newsResponse.value)


    }

    private fun getMockResponse(): ApiResponse<ArrayList<RowsItem>> {
        return ApiResponse("Response Success", getList())
    }

    private fun getList(): ArrayList<RowsItem> {
        val rowsItem = RowsItem("ImageHref", "Desc", "Title")
        val list = ArrayList<RowsItem>()
        list.add(rowsItem)
        return list
    }


}